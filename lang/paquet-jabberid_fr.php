<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Module: paquet-jabberid
// Langue: fr

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// J
	'jabberid_description' => 'Permet de disposer d\'un champ "JID" (adresse Jabber) sur les auteurs de SPIP.

Jabber est un système de messagerie instantanée (à la manière de MSN) et décentralisé (comme les mails).
Il est basé sur le protocole ouvert XMPP.

Si vous ne connaissez pas encore ce réseau de messagerie, vous pourrez trouver des informations sur le
site francophone [JabberFR->http://jabberfr.org/].

Ce plugin fournit un champ supplémentaire dans le formulaire d\'édition des informations d\'un auteur. La
balise correspondante est <code>#JID</code> (appellation standard de l\'identifiant Jabber).',
	'jabberid_slogan' => 'Une adresse Jabber pour les auteurs',
);
?>